<?php
// +----------------------------------------------------------------------
// | PHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
return [
    'http_server_host' => '0.0.0.0',
    'http_server_port' => 9999,
    'http_work_num' => '1',
    'http_max_request' => '5000',
    'http_max_conn' => '200',
    'http_dispatch_mode' => '3',
    'http_upload_tmp_dir' => '/temp/'
];
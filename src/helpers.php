<?php
// +----------------------------------------------------------------------
// | JYPHP [ JUST YOU ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2017 http://www.jyphp.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Albert <albert_p@foxmail.com>
// +----------------------------------------------------------------------
if (!function_exists('dir_each')) {

    /**
     * 遍历目录
     * @param string $dir 目录路径
     * @param callable $func
     * @throws Exception
     */
    function dir_each(string $dir, callable $func)
    {
        if (!is_dir($dir)) {
            throw new \Exception("${dir} not is directory");
        }
        $res = opendir($dir);
        while (($file = readdir($res)) !== false) {
            call_user_func($func, $file);
        }
    }

}